#!/usr/bin/env bash

for i in `seq 1 8`;
do
    echo "---[ $i ]---"
    php parser.php < ./tests/input$i > ./output/output$i\.xml
    diff -Naur ./tests/output$i\.xml ./output/output$i\.xml > ./output/diff$i
done