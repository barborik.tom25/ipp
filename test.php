<?php

$ch = curl_init("https://spravce.dietfreshmenu.cz/communication/order");
$data = ["a", "b", 0, 1];
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_HTTPHEADER, ["COMMUNICATION_TOKEN: test"]);

$result = curl_exec($ch);
if (curl_errno($ch)) {
    exit(curl_errno($ch));
}

if (curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200){
    curl_close($ch);
    exit(-1);
}

curl_close($ch);
echo $result;